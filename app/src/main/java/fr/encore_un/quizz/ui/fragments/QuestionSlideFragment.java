package fr.encore_un.quizz.ui.fragments;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import fr.encore_un.quizz.R;
import fr.encore_un.quizz.adapters.ResponsesAdapters;
import fr.encore_un.quizz.model.Question;
import fr.encore_un.quizz.singletons.ResponsesValidated;

public class QuestionSlideFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    private ListView mResponseListView;

    private Question mQuestion;

    private int mPosition;


    public static QuestionSlideFragment newInstance(Question question,int position){
        QuestionSlideFragment questionSlideFragment = new QuestionSlideFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("question",question);
        bundle.putInt("position",position);
        questionSlideFragment.setArguments(bundle);
        return questionSlideFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_question_slide, container, false);
        mQuestion = (Question) this.getArguments().getSerializable("question");
        mPosition = (this.getArguments().getInt("position")) + 1;
        // defined the question title
        TextView questionText = (TextView) rootView.findViewById(R.id.questionText);
        int total = ResponsesValidated.getInstance().getNbTotalQuestions();
        questionText.setText(mQuestion.getQuestionText() + " (Q"+mPosition+"/"+total+")");
        questionText.setPaintFlags(questionText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        questionText.setTypeface(null, Typeface.BOLD);
        mResponseListView = (ListView) rootView.findViewById(R.id.responseListView);
        // set the adapater to display the possible responses
        ResponsesAdapters adapter = new ResponsesAdapters(mQuestion.getResponses(), getActivity());
        mResponseListView.setAdapter(adapter);

        CheckBox validateCheckbox = (CheckBox) rootView.findViewById(R.id.validateResponse);
        validateCheckbox.setOnCheckedChangeListener(this);

        return rootView;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
        {ResponsesValidated.getInstance().incrementResponsesValidated();}
        else
        {ResponsesValidated.getInstance().decrementResponsesValidated();}
    }

}