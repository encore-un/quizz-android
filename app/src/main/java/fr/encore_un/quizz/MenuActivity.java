package fr.encore_un.quizz;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import fr.encore_un.quizz.databases.QuizzAndroidDatabasesManager;
import fr.encore_un.quizz.model.Quizz;
import fr.encore_un.quizz.services.LoginService;
import fr.encore_un.quizz.services.QuizzService;

import lombok.AllArgsConstructor;
import lombok.Data;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String username = LoginService.getLogin();
        if(username == null || username.isEmpty()){
            // redirection if not logged
            redirectToLoginActivity();
            return;
        }
        setContentView(R.layout.activity_menu);
        TextView userNameView = (TextView)findViewById(R.id.userName);
        userNameView.setText(username);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.quizz_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case(R.id.logout):
                LoginService.removeLogin();
                redirectToLoginActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void redirectToLoginActivity(){
        Intent i = new Intent(this,QuizzLoginActivity.class);
        startActivity(i);
    }

    public void onQuizzClick(View view){
        View rootView = view.getRootView();
        RelativeLayout quizzAskLayout = (RelativeLayout)rootView.findViewById(R.id.askQuizzLayout);
        quizzAskLayout.setVisibility(View.VISIBLE);
        Button quizzButton = (Button)view.findViewById(R.id.quizzButton);
        quizzButton.setVisibility(View.GONE);
    }

    public void onResultsClick(View view){
        Intent i = new Intent(this, AllResultsActivity.class);
        startActivity(i);
    }

    public void getQuizz(View view) {
        EditText editQuizzId = (EditText) findViewById(R.id.quizzId);
        String quizzId = editQuizzId.getText().toString();
        if(quizzId.isEmpty() || quizzId == null){
            Toast.makeText(MenuActivity.this, "Quizz ID can't be empty or null", Toast.LENGTH_LONG).show();
            return;
        }
        QuizzParams quizzParams = new QuizzParams(this, editQuizzId.getText().toString());
        new GetQuizzAsyncTask().execute(quizzParams);
    }


    public class GetQuizzAsyncTask extends AsyncTask<QuizzParams, Void, Quizz> {
        @Override
        protected Quizz doInBackground(QuizzParams...params) {
            return QuizzService.getQuizz(params[0].getContext(), params[0].getId());
        }

        @Override
        protected void onPostExecute(Quizz quizz) {
            if(quizz == null)
            {
                Toast.makeText(MenuActivity.this, "Quizz ID not found, try again", Toast.LENGTH_LONG).show();
                return;
            }
            Intent quizzIntent = new Intent(MenuActivity.this, QuizzActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("quizz", quizz);
            quizzIntent.putExtras(bundle);
            startActivity(quizzIntent);
        }
    }

    @Data
    @AllArgsConstructor(suppressConstructorProperties = true)
    public class QuizzParams {
        private Context context;
        private String id;
    }
}
