package fr.encore_un.quizz;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import fr.encore_un.quizz.model.Question;
import fr.encore_un.quizz.model.Quizz;
import fr.encore_un.quizz.model.Response;
import fr.encore_un.quizz.singletons.QuizzTimer;
import fr.encore_un.quizz.singletons.ResponsesValidated;
import fr.encore_un.quizz.transformers.ZoomOutPageTransformer;
import fr.encore_un.quizz.ui.fragments.QuestionSlideFragment;

public class QuizzActivity extends FragmentActivity{

    private static int NUM_PAGES;

    // Pager widget : handles animation and allows swiping horizontally
    private ViewPager mPager;

    // Pager adapter : provides the page to the view pager widget
    private PagerAdapter mPagerAdapter;

    private Quizz mQuizz;

    private List<Question> mQuestions;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizz);

        // start timer
        QuizzTimer.start();

        // get quizz instance in the bundle
        Bundle bundle = this.getIntent().getExtras();
        mQuizz = (Quizz) bundle.getSerializable("quizz");
        mQuestions = mQuizz.getQuestions();

        // the number of pages corresponds to the number of questions
        NUM_PAGES = mQuestions.size();
        ResponsesValidated.getInstance().setNbTotalQuestions(NUM_PAGES);
        // reset number of confirmed responses to 0 when starting activity
        // same for the number of correct responses for the user
        ResponsesValidated.getInstance().setNbResponsesValidated(0);
        ResponsesValidated.getInstance().setNbUserCorrectResponses(0);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
    }

    public void onConfirm(View view){
        if(mQuestions.size() != ResponsesValidated.getInstance().getNbResponsesValidated())
        {
            Toast.makeText(this, "All your answers must be confirmed before submitting",Toast.LENGTH_LONG).show();
            return;
        }

        QuizzTimer.stop();

        // set timer in seconds
        mQuizz.setTimer(QuizzTimer.getTime() / 1000);

        Intent resultIntent = new Intent(this, ResultActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("quizz",mQuizz);
        resultIntent.putExtras(bundle);
        startActivity(resultIntent);
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            final QuestionSlideFragment questionSlideFragment = QuestionSlideFragment.newInstance(mQuestions.get(position),position);
            return questionSlideFragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

}


