package fr.encore_un.quizz;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import fr.encore_un.quizz.adapters.AllResultsAdapter;
import fr.encore_un.quizz.model.Result;
import fr.encore_un.quizz.services.LoginService;
import fr.encore_un.quizz.services.ResultService;
import lombok.AllArgsConstructor;
import lombok.Data;

public class AllResultsActivity extends AppCompatActivity{

    private List<Result> mResults;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_results);

        new GetAllResultsAsyncTask()
                        .execute(new GetAllResultsParams(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.quizz_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case(R.id.logout):
                LoginService.removeLogin();
                redirectToLoginActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void redirectToLoginActivity(){
        Intent i = new Intent(this,QuizzLoginActivity.class);
        startActivity(i);
    }

    public void onBackMenu(View view){
        Intent i = new Intent(this, MenuActivity.class);
        startActivity(i);
    }

    @Data
    @AllArgsConstructor(suppressConstructorProperties = true)
    public class GetAllResultsParams {
        private Context context;
    }

    public class GetAllResultsAsyncTask extends AsyncTask<GetAllResultsParams, Void, List<Result>> {
        @Override
        protected List<Result> doInBackground(GetAllResultsParams...params) {
            return ResultService.getAllResults(AllResultsActivity.this);
        }

        @Override
        protected void onPostExecute(List<Result> results) {
            ListView listView = (ListView) findViewById(R.id.allResultList);
            AllResultsAdapter adapter = new AllResultsAdapter(results, AllResultsActivity.this);
            listView.setAdapter(adapter);
        }
    }

}
