package fr.encore_un.quizz.services;

import fr.encore_un.quizz.QuizzApplication;
import android.content.Context;
import android.content.SharedPreferences;

public class LoginService {

    private static final String QUIZZ_KEY = "QuizzSharedPrefs";
    private static final String LOGIN_KEY = "PrefLogin";
    @SuppressWarnings("unused")
    private static SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences(QUIZZ_KEY, Context.MODE_PRIVATE);
    }

    private static SharedPreferences getSharedPreferences(){
        return QuizzApplication.getContext().getSharedPreferences(QUIZZ_KEY, Context.MODE_PRIVATE);
    }

    public static String getLogin(){
        final SharedPreferences prefs = getSharedPreferences();
        return prefs.getString(LOGIN_KEY, null);
    }

    public static void setLogin(String login){
        final SharedPreferences prefs = getSharedPreferences();
        prefs.edit().putString(LOGIN_KEY, login).apply();
    }

    public static void removeLogin(){
        final SharedPreferences prefs = getSharedPreferences();
        prefs.edit().remove(LOGIN_KEY).apply();
    }
}
