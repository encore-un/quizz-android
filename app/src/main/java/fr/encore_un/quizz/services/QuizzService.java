package fr.encore_un.quizz.services;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.encore_un.quizz.model.Question;
import fr.encore_un.quizz.model.Quizz;
import fr.encore_un.quizz.model.Response;
import lombok.AllArgsConstructor;
import lombok.Data;

public class QuizzService {


    @Data
    @AllArgsConstructor(suppressConstructorProperties = true)
    public class QuizzParams {
        private String id;
        private Context context;
    }

    public static Quizz getQuizz(Context context, String id) {
        Quizz finalQuizz = null;

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("quizz.json")));
            Quizz[] quizzArray = new Gson().fromJson(reader, Quizz[].class);
            List<Quizz> quizzList = Arrays.asList(quizzArray);
            for (Quizz quizz:quizzList ) {
                if(id.toUpperCase().equals(quizz.getId().toUpperCase())){
                    finalQuizz = quizz;
                    break;
                }
            }

        } catch (IOException e) {

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {

                }
            }
        }

        return finalQuizz;
    }

}


