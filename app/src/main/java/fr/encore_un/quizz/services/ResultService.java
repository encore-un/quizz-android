package fr.encore_un.quizz.services;

import android.content.Context;

import java.util.List;

import fr.encore_un.quizz.databases.QuizzAndroidDatabasesManager;
import fr.encore_un.quizz.model.Result;

public class ResultService {
    public static long addResult(Context context, Result result) {
        return QuizzAndroidDatabasesManager.addResult(context, result);
    }

    public static List<Result> getAllResults(Context context){
        return QuizzAndroidDatabasesManager.getStoredResults(context);
    }
}
