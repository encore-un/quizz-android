package fr.encore_un.quizz.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import fr.encore_un.quizz.model.Result;

public class QuizzAndroidDatabasesManager {

    public static Result resultFromCursor(Cursor c){

        if (null != c){
            Result.ResultBuilder resultBuilder = Result.builder();

            // Retrieve the date
            if (c.getColumnIndex(QuizzAndroidDatabaseContract.DATE_TIMESTAMP) >= 0){
                resultBuilder.date(new java.sql.Date(c.getLong(c.getColumnIndex(QuizzAndroidDatabaseContract.DATE_TIMESTAMP))));
            }

            // Retrieve the question count
            if (c.getColumnIndex(QuizzAndroidDatabaseContract.QUESTIONS_COUNT) >= 0){
                resultBuilder.nbQuestions(c.getInt(c.getColumnIndex(QuizzAndroidDatabaseContract.QUESTIONS_COUNT)));
            }

            // Retrieve the correct answer count
            if (c.getColumnIndex(QuizzAndroidDatabaseContract.CORRECT_ANSWERS_COUNT) >= 0){
                resultBuilder.nbCorrectAnswers(c.getInt(c.getColumnIndex(QuizzAndroidDatabaseContract.CORRECT_ANSWERS_COUNT)));
            }

            // Retrieve the timer
            if (c.getColumnIndex(QuizzAndroidDatabaseContract.TIMER) >= 0){
                resultBuilder.timer(c.getInt(c.getColumnIndex(QuizzAndroidDatabaseContract.TIMER)));
            }

            // Retrieve the user ID
            if (c.getColumnIndex(QuizzAndroidDatabaseContract.USER_ID) >= 0){
                resultBuilder.userId(c.getString(c.getColumnIndex(QuizzAndroidDatabaseContract.USER_ID)));
            }

            // Retrieve the user ID
            if (c.getColumnIndex(QuizzAndroidDatabaseContract.QUIZZ_ID) >= 0){
                resultBuilder.quizzId(c.getString(c.getColumnIndex(QuizzAndroidDatabaseContract.QUIZZ_ID)));
            }

            return resultBuilder.build();
        }
        return null;
    }

    public static ContentValues resultToContentValues(Result result){
        final ContentValues values = new ContentValues();

        // Set the date
        if(result.getDate() != null) {
            values.put(QuizzAndroidDatabaseContract.DATE_TIMESTAMP, result.getDate().getTime());
        }

        values.put(QuizzAndroidDatabaseContract.QUESTIONS_COUNT, result.getNbQuestions());

        values.put(QuizzAndroidDatabaseContract.CORRECT_ANSWERS_COUNT, result.getNbCorrectAnswers());

        values.put(QuizzAndroidDatabaseContract.TIMER, result.getTimer());

        values.put(QuizzAndroidDatabaseContract.USER_ID, result.getUserId());

        values.put(QuizzAndroidDatabaseContract.QUIZZ_ID, result.getQuizzId());

        return values;
    }

    public static synchronized void clearDatabase(Context context){
        QuizzAndroidDatabaseHelper dbHelper = new QuizzAndroidDatabaseHelper(context);

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        database.execSQL("DELETE FROM "+QuizzAndroidDatabaseContract.TABLE_RESULTS);
    }


    public static synchronized List<Result> getStoredResults(Context context){
        final List<Result> results = new ArrayList<>();

        QuizzAndroidDatabaseHelper dbHelper = new QuizzAndroidDatabaseHelper(context);

        SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor c = database.query(
                QuizzAndroidDatabaseContract.TABLE_RESULTS,
                QuizzAndroidDatabaseContract.PROJECTION_FULL,
                null,
                null,
                null,
                null,
                null
        );

        while(c.moveToNext()){
            results.add(resultFromCursor(c));
        }

        return results;
    }

    public static synchronized long addResult(Context context, Result result){
        QuizzAndroidDatabaseHelper dbHelper = new QuizzAndroidDatabaseHelper(context);

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        return database.insert(QuizzAndroidDatabaseContract.TABLE_RESULTS, null, resultToContentValues(result));
    }

    public static synchronized void dropDatabase(Context context){
        context.deleteDatabase(QuizzAndroidDatabaseContract.TABLE_RESULTS);
    }
}
