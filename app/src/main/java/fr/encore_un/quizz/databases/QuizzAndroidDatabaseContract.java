package fr.encore_un.quizz.databases;

import android.net.Uri;
import android.provider.BaseColumns;

public class QuizzAndroidDatabaseContract implements BaseColumns {

    // Field names
    public static final String DATE_TIMESTAMP = "DATE";
    public static final String TIMER = "TIMER";
    public static final String USER_ID = "ID_USER";
    public static final String QUESTIONS_COUNT = "QUESTIONS_COUNT";
    public static final String CORRECT_ANSWERS_COUNT = "CORRECT_ANSWERS_COUNT";
    public static final String QUIZZ_ID = "QUIZZ_ID";

    // Table name
    public static final String TABLE_RESULTS = "results";

    // Table scripts creation
    private static final String TABLE_GENERIC_CREATE_SCRIPT_PREFIX = "CREATE TABLE IF NOT EXISTS ";
    private static final String TABLE_IMAGES_CREATE_SCRIPT_SUFFIX = "(" + _ID + " INTEGER PRIMARY KEY, " +
            DATE_TIMESTAMP + " INTEGER, " +
            TIMER + " INTEGER, " +
            USER_ID + " VARCHAR(255), "+
            QUESTIONS_COUNT + " INTEGER, "+
            QUIZZ_ID + " VARCHAR(200), "+
            CORRECT_ANSWERS_COUNT + " INTEGER)";

    public static final String TABLE_RESULTS_CREATE_SCRIPT = TABLE_GENERIC_CREATE_SCRIPT_PREFIX +
            TABLE_RESULTS + TABLE_IMAGES_CREATE_SCRIPT_SUFFIX;

    // The projections
    public static final String[] PROJECTION_FULL = new String[]{
            _ID,
            DATE_TIMESTAMP,
            TIMER,
            USER_ID,
            QUESTIONS_COUNT,
            CORRECT_ANSWERS_COUNT,
            QUIZZ_ID
    };
}
