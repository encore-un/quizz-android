package fr.encore_un.quizz.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class QuizzAndroidDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "results.db";
    private static final int DATABASE_VERSION = 1;

    public QuizzAndroidDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QuizzAndroidDatabaseContract.TABLE_RESULTS_CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + QuizzAndroidDatabaseContract.TABLE_RESULTS);
        onCreate(db);
    }
}
