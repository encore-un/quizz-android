package fr.encore_un.quizz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import fr.encore_un.quizz.services.LoginService;

public class QuizzLoginActivity extends Activity implements View.OnClickListener {
    private EditText mLoginEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String username = LoginService.getLogin();
        if(username != null && !username.isEmpty()){
            // redirection if already logged
            Intent i = new Intent(this,MenuActivity.class);
            startActivity(i);
        }
        setContentView(R.layout.activity_login);
        mLoginEditText = (EditText) findViewById(R.id.loginEditText);
        findViewById(R.id.loginButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (TextUtils.isEmpty(mLoginEditText.getText())) {
            Toast.makeText(this, "Empty Login!", Toast.LENGTH_SHORT).show();
            return;
        }

        LoginService.setLogin(mLoginEditText.getText().toString());

        final Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
}
