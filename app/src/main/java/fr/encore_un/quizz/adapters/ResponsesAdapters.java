package fr.encore_un.quizz.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import fr.encore_un.quizz.R;
import fr.encore_un.quizz.model.Response;

public class ResponsesAdapters extends BaseAdapter implements View.OnClickListener {

    private List<Response> mResponses;
    private Context mContext;
    private LayoutInflater mInflater;

    public ResponsesAdapters(List<Response> responses, Context context){
        mResponses = responses;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return mResponses != null ? mResponses.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mResponses != null ? mResponses.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.response_item, null);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // Get current item
        final Response response = (Response) getItem(position);
        // Set differents infos
        holder.response.setText(response.getResponseText());
        holder.checkBox.setChecked(response.getUserReponse());
        convertView.setOnClickListener(this);

        return convertView;
    }

    @Override
    public void onClick(View v) {
        ViewHolder holder = (ViewHolder) v.getTag();
        Boolean newState = !holder.checkBox.isChecked();
        updateResponseModel(newState, holder);
    }

    private void updateResponseModel(Boolean newState, ViewHolder holder){
        holder.checkBox.setChecked(newState);
        for (Response resp: mResponses) {
            if(resp.getResponseText().equals(holder.response.getText().toString())){
                resp.setUserReponse(newState);
                return;
            }
        }
    }

    private class ViewHolder {

        public TextView response;
        public CheckBox checkBox;

        public ViewHolder(View view){
            response = (TextView) view.findViewById(R.id.responseTextItem);
            checkBox = (CheckBox) view.findViewById(R.id.responseCheckBoxItem);
        }
    }

}
