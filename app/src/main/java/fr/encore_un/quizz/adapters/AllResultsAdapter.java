package fr.encore_un.quizz.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import fr.encore_un.quizz.R;
import fr.encore_un.quizz.model.Result;


public class AllResultsAdapter extends BaseAdapter {

    private List<Result> mResults;
    private Context mContext;
    private LayoutInflater mInflater;

    public AllResultsAdapter(List<Result> results, Context context){
        mResults = results;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mResults == null ? null : mResults.size();
    }

    @Override
    public Object getItem(int position) {
        return mResults == null ? null : mResults.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.all_results_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Result result = (Result) getItem(position);

        holder.userId.setText(result.getUserId() + " ");
        holder.quizzId.setText("Id: " + result.getQuizzId());
        holder.timer.setText("Time : "+Integer.toString(result.getTimer()) + " seconds");
        holder.timer.setPaintFlags(holder.score.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.timer.setTextColor(Color.parseColor("#0277BD"));
        holder.timer.setTypeface(null, Typeface.BOLD);
        String dateFormat = (String) android.text.format.DateFormat.format("yyyy-MM-dd HH:mm", result.getDate());
        holder.date.setText(dateFormat);
        holder.score.setText("Score : " +result.getNbCorrectAnswers() + "/" + result.getNbQuestions());
        holder.score.setPaintFlags(holder.score.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.score.setTextColor(Color.parseColor("#0277BD"));
        holder.score.setTypeface(null, Typeface.BOLD);

        return convertView;

    }

    private class ViewHolder {
        TextView userId;
        TextView date;
        TextView quizzId;
        TextView score;
        TextView timer;

        ViewHolder(View view) {
            userId = (TextView) view.findViewById(R.id.userId);
            date = (TextView) view.findViewById(R.id.date);
            quizzId = (TextView) view.findViewById(R.id.quizz_id_result);
            score= (TextView) view.findViewById(R.id.score);
            timer = (TextView) view.findViewById(R.id.timer);
        }
    }
}
