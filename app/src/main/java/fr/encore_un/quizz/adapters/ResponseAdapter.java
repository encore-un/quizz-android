package fr.encore_un.quizz.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.encore_un.quizz.model.Question;
import fr.encore_un.quizz.R;
import fr.encore_un.quizz.model.Response;
import fr.encore_un.quizz.model.Result;
import fr.encore_un.quizz.model.ResultView;
import fr.encore_un.quizz.singletons.ResponsesValidated;

public class ResponseAdapter extends BaseAdapter {
    public List<ResultView> mResultViews;
    public Context mContext;
    public LayoutInflater mInflater;

    public ResponseAdapter(List<ResultView> questions, Context context) {
        this.mResultViews = questions;
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return null != mResultViews ? mResultViews.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null != mResultViews ? mResultViews.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.response, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ResultView resultView = (ResultView) getItem(position);

        holder.correctResponses.setText("Correct answers : " + resultView.getCorrectResponsesString());
        holder.userResponses.setText("Your answers : " + resultView.getUserResponseString());
        holder.questionText.setText(resultView.getQuestionText());
        holder.questionText.setPaintFlags(holder.questionText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.questionText.setTypeface(null, Typeface.BOLD);
        holder.validity.setText(resultView.getValidityText());
        holder.validity.setPaintFlags(holder.validity.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if(resultView.getValidity()){
            holder.correctResponses.setVisibility(View.GONE);
            holder.validity.setTextColor(Color.parseColor("#4DB6AC"));
        }
        else{
            holder.validity.setTextColor(Color.parseColor("#D32F2F"));
            holder.correctResponses.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    private class ViewHolder {
        TextView questionText;
        TextView userResponses;
        TextView correctResponses;
        TextView validity;

        public ViewHolder(View view) {
            questionText = (TextView) view.findViewById(R.id.questionText);
            userResponses = (TextView) view.findViewById(R.id.userResponses);
            correctResponses = (TextView) view.findViewById(R.id.correctResponses);
            validity = (TextView) view.findViewById(R.id.validityText);
        }
    }
}
