package fr.encore_un.quizz.singletons;


import android.util.Log;

// singleton
public class ResponsesValidated {

    private static ResponsesValidated mInstance= null;

    private int nbTotalQuestions = 0;

    private int nbResponsesValidated = 0;

    private int nbUserCorrectResponses = 0;

    protected ResponsesValidated(){}

    public static synchronized ResponsesValidated getInstance(){
        if(null == mInstance){
            mInstance = new ResponsesValidated();
        }
        return mInstance;
    }

    public int getNbTotalQuestions() {
        return nbTotalQuestions;
    }

    public void setNbTotalQuestions(int nbTotalQuestions) { this.nbTotalQuestions = nbTotalQuestions; }

    public void setNbResponsesValidated(int nbResponsesValidated) {this.nbResponsesValidated = nbResponsesValidated; }

    public int getNbResponsesValidated() {
        return nbResponsesValidated;
    }

    public void incrementResponsesValidated(){nbResponsesValidated++; }

    public void decrementResponsesValidated(){ nbResponsesValidated--;}

    public int getNbUserCorrectResponses() {return nbUserCorrectResponses; }

    public void setNbUserCorrectResponses(int nbUserCorrectResponses) {this.nbUserCorrectResponses = nbUserCorrectResponses; }

    public void incrementUserCorrectResponses(){
        nbUserCorrectResponses++;}
}
