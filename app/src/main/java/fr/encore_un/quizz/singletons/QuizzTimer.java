package fr.encore_un.quizz.singletons;


public class QuizzTimer {
    private static long begin = -1L;
    private static long end = -1L;

    public static void start() {
        begin = System.currentTimeMillis();

        if (end != -1L) end = -1L;
    }

    public static void stop() {
        end = System.currentTimeMillis();
    }

    public static int getTime() {
        if (end != -1L && begin != -1L) {
            return (int) (end - begin);
        } else {
            return -1;
        }
    }
}
