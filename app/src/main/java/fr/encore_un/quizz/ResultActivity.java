package fr.encore_un.quizz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.encore_un.quizz.services.LoginService;
import fr.encore_un.quizz.services.ResultService;
import fr.encore_un.quizz.model.Question;
import fr.encore_un.quizz.model.Quizz;
import fr.encore_un.quizz.adapters.ResponseAdapter;
import fr.encore_un.quizz.model.Response;
import fr.encore_un.quizz.model.Result;
import fr.encore_un.quizz.model.ResultView;
import fr.encore_un.quizz.singletons.ResponsesValidated;
import lombok.AllArgsConstructor;
import lombok.Data;


public class ResultActivity extends Activity implements View.OnClickListener {

    private Quizz mQuizz;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Bundle bundle = this.getIntent().getExtras();
        mQuizz = (Quizz) bundle.getSerializable("quizz");

        TextView usernameView = (TextView) findViewById(R.id.username);
        usernameView.setText(LoginService.getLogin());

        TextView quizzIdView = (TextView) findViewById(R.id.quizz_id);
        quizzIdView.setText("Id : " + mQuizz.getId());

        TextView quizzTimerView = (TextView) findViewById(R.id.quizz_time);
        quizzTimerView.setText("Quizz done in " + mQuizz.getTimer() + " seconds");

        Button button = (Button) findViewById(R.id.result_to_menu_button);
        button.setOnClickListener(this);

        new ComputeResultsAsyncTask().execute(mQuizz.getQuestions());
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, MenuActivity.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed(){
        // disable back button when ending a quizz
    }

    public class ComputeResultsAsyncTask extends AsyncTask<List<Question>, Void,List<ResultView>>{

        @Override
        protected List<ResultView> doInBackground(List<Question> ... params) {
            List<ResultView> resultViews = new ArrayList<>();
            for (Question question: params[0]) {
                List<String> correctAnswers = new ArrayList<>();
                List<String> userResponses = new ArrayList<>();
                String userRespString = "";
                String correctRespString = "";

                for (Response r : question.getResponses()) {
                    if(r.getValidity()){
                        correctRespString += r.getResponseText() + " / ";
                        correctAnswers.add(r.getResponseText());
                    }

                    if (r.getUserReponse()) {
                        userRespString += r.getResponseText() + " / ";
                        userResponses.add(r.getResponseText());
                    }
                }

                Boolean isUserResponseCorrect = false;
                if(userResponses.size() == correctAnswers.size()){
                    // check answers oly if we have the same number
                    for (String resp : userResponses){
                        if(correctAnswers.contains(resp))
                        {isUserResponseCorrect = true;}
                        else
                        {
                            isUserResponseCorrect = false;
                            break;
                        }
                    }
                }
                if(!isUserResponseCorrect){

                    resultViews.add(ResultView.builder().correctResponsesString(correctRespString)
                            .questionText(question.getQuestionText())
                            .userResponseString(userRespString)
                            .validityText("Bad answer ! ")
                            .validity(false).build());
                }
                else{
                    ResponsesValidated.getInstance().incrementUserCorrectResponses();
                    resultViews.add(ResultView.builder().correctResponsesString(correctRespString)
                            .questionText(question.getQuestionText())
                            .userResponseString(userRespString)
                            .validityText("Correct answer ! ")
                            .validity(true).build());
                }
            }
            return resultViews;
        }

        @Override
        protected void onPostExecute(List<ResultView> results) {
            ListView listView = (ListView) findViewById(R.id.questionsResultListViewer);
            ResponseAdapter adapter = new ResponseAdapter(results, ResultActivity.this);
            listView.setAdapter(adapter);

            Result storeResult = Result.builder().userId(LoginService.getLogin())
                    .timer(mQuizz.getTimer())
                    .date(new Date())
                    .nbCorrectAnswers(ResponsesValidated.getInstance().getNbUserCorrectResponses())
                    .nbQuestions(mQuizz.getQuestions().size())
                    .quizzId(mQuizz.getId()).build();

            // launch result service to store in database
            new AddResultAsyncTask().execute(new AddResultParams(ResultActivity.this,storeResult));
        }
    }

    @Data
    @AllArgsConstructor(suppressConstructorProperties = true)
    public class AddResultParams {
        private Context context;
        private Result result;
    }

    public class AddResultAsyncTask extends AsyncTask<AddResultParams, Void, Long> {
        @Override
        protected Long doInBackground(AddResultParams...params) {
            return ResultService.addResult(params[0].getContext(), params[0].getResult());
        }
    }
}
