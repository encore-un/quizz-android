package fr.encore_un.quizz.model;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Builder;

@Data
@Builder
@AllArgsConstructor(suppressConstructorProperties = true)
public class Question implements Serializable {
    private String questionText;
    private List<Response> responses;
}
