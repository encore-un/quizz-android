package fr.encore_un.quizz.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Builder;

@Data
@Builder
@AllArgsConstructor(suppressConstructorProperties = true)
public class Response implements Serializable {
    private String responseText;
    private Boolean validity;
    private Boolean userReponse = false;

    @Override
    public String toString(){ return responseText;}
}
