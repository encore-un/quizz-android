package fr.encore_un.quizz.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Builder;

@Data
@Builder
@AllArgsConstructor(suppressConstructorProperties = true)
public class ResultView {
    private String userResponseString;
    private String correctResponsesString;
    private String validityText;
    private Boolean validity;
    private String questionText;
}
