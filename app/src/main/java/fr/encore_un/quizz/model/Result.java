package fr.encore_un.quizz.model;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Builder;

@Data
@Builder
@AllArgsConstructor(suppressConstructorProperties = true)
public class Result implements Serializable {
    private String userId;
    private int timer;
    private Date date;
    private int nbQuestions;
    private int nbCorrectAnswers;
    private String quizzId;
}
